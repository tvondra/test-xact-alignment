PGXACT cacheline alignment benchmarks
=====================================

Scripts running a series of tests with various workloads, measuring the impact
of PGXACT cacheline alignment patch, posted in CF2017-03:

* https://commitfest.postgresql.org/13/974/
* http://www.postgresql.org/message-id/attachment/49134/pgxact-align-2.patch

The resuls available in this repository are for these workloads:

* pgbench r/o and r/w (-M simple)
* pgbench r/o and r/w (-M prepared)
* 90% reads, 10% writes (-M prepared)
* skewed distribution (-M prepared)
* skewed distribution with skipping (-M prepared)

The workloads were performed on scales 100 (fits into `shared_buffers`), 1000
(fits into RAM) and 10000 (hits I/O). The client counts were 8, 16, 32, 48, 64
and 80 (the machine has 16 physical cores, 32 with HT).

For each scale there was a 10-minute warmup, followed by 5 runs for each client
count. Each run was 5 minutes, and there was an explicit checkpoint before each
run (to minimize noise in read-write tests).

The raw tps results are available as a CSV file. There's a lot of data collected
for each run, including database statistics at 1-second granularity, etc.


Running the tests
-----------------

To rerun the tests, you need to do about this:

1. prepare PostgreSQL binaries with and without the patch (the scripts need
   them at the same time)

2. modify the shell scripts driving the benchmark (there's a bunch of paths,
   and also test parameters - number of runs, durations, scales, client counts,
   data directory path, and so on)

3. run the main shell script (`run.sh`) and wait

4. build CSV with results using the `results.sh` scipt
