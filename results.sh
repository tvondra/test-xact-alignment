for t in pgbench-ro pgbench-ro-simple pgbench-rw pgbench-rw-simple pgbench-reads-writes pgbench-skewed pgbench-skewed-n; do

	for h in master xact; do

		for s in 100 1000; do

			for c in 8 16 32 48 64 80; do

				for r in 1 2 3 4 5; do

					if [ ! -d "$t/$h/$s/benchmark/$c/$r" ]; then
				                continue
					fi

					tps=`grep including $t/$h/$s/benchmark/$c/$r/pgbench.log | awk '{print $3}' | sed 's/\..*//'`

					wal_min=`head -n 1 $t/$h/$s/benchmark/$c/$r/xlog.csv | awk '{print $8}'`
					wal_max=`tail -n 2 $t/$h/$s/benchmark/$c/$r/xlog.csv | head -n 1 | awk '{print $8}'`

					echo $t $h $s $c $r $tps $((wal_max-wal_min))

				done
			done
		done
	done
done
