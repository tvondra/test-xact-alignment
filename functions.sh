function init_cluster
{
        d=$1
        s=$2
	opts=$3

        mkdir -p $d/$s

        killall -9 postgres > /dev/null 2>&1

        sleep 15

        rm -Rf /mnt/data/pgdata

        pg_ctl -D /mnt/data/pgdata -o "$opts" init > $d/$s/init.log 2>&1

        cp postgresql.conf /mnt/data/pgdata

        pg_ctl -D /mnt/data/pgdata -l $d/$s/pg.log start > /dev/null 2>&1

        psql postgres -c "select * from pg_settings" > $d/$s/settings.log 2>&1

        createdb pgbench

        pgbench -i -s $s pgbench > $d/$s/bench.init.log 2>&1
}

function stop_cluster
{
	pg_ctl -D /mnt/data/pgdata -w -t 3600 stop > /dev/null 2>&1
}

function stats_collector_start
{
	x=$1

        ./collect-stats.sh $x &
        ./collect-wait-events.sh $x &
}

function stats_collector_stop
{
	kill $(jobs -rp)
	wait $(jobs -rp) > /dev/null 2>&1
}

