#!/bin/sh

./run-pgbench-ro.sh > pgbench-ro.log 2>&1

./run-pgbench-rw.sh > pgbench-rw.log 2>&1

./ run-pgbench-skewed-n.sh > pgbench-skewed-n.log 2>&1

./run-pgbench-skewed.sh > pgbench-skewed.log 2>&1

./run-pgbench-reads-writes.sh > pgbench-reads-writes.log 2>&1

./run-pgbench-ro-simple.sh > pgbench-ro-simple.log 2>&1

./run-pgbench-rw-simple.sh > pgbench-rw-simple.log 2>&1
